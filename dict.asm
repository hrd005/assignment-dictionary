%define PTR_SIZE 8

global find_word

extern string_equals

section .text

find_word:                  ; rdi -- pointer to null-terminated string
                            ; rsi -- pointer to dictionary
    .loop:
        test rsi, rsi
        jz  .end

        push rdi            ; save registers
        push rsi

        add rsi, PTR_SIZE   ; first thing we store -- pointer to next elem, we do not it
        call string_equals  ; rdi = rdi, rsi fixed

        pop rsi             ; restore registers
        pop rdi

        cmp rax, 1          ; 1 for equal strings
        je  .end

        mov rsi, [rsi]      ; go to next elem
        test rsi, rsi       ; is there any elem
        jnz .loop

    .end:
        mov rax, rsi        ; ret pointer to entry start
        ret                 ; rsi is always zero when entry is not found
