%define BUFFER_SIZE 256
%define PTR_SIZE 8

%include "colon.inc"
%include "words.inc"

section .rodata
read_err_text: db "stdin read error", 0
word_not_found: db "Word is not found", 0

section .text

extern find_word

extern read_word
extern print_string
extern print_newline
extern string_length
extern exit

global _start
_start:

    sub rsp, BUFFER_SIZE        ; allocate buffer on stack
    mov rdi, rsp                ; move start buffer to rdi
    mov rsi, BUFFER_SIZE
    call read_word

    test rax, rax               ; is there word?
    jz  .read_error

    mov rdi, rax
    mov rsi, lptr

    call find_word

    test rax, rax               ; is read word found?
    jz  .not_found

    push rax                    ; store pointer

    add rax, PTR_SIZE           ; calculate offset
    mov rdi, rax
    call string_length

    pop rdi                     ; pointer to start of entry
    add rdi, PTR_SIZE           ; pointer to start of key
    add rdi, rax                ; pointer to start of result
    inc rdi
    call print_string
    call print_newline

    add rsp, BUFFER_SIZE    ; restore stack
    call exit

    .read_error:
        mov rdi, read_err_text
        call print_string
        call print_newline

        add rsp, BUFFER_SIZE
        mov rdi, 1
        call exit

    .not_found:
        mov rdi, word_not_found
        call print_string
        call print_newline

        add rsp, BUFFER_SIZE
        mov rdi, 2
        call exit
