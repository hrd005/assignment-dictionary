section .text

%define EXIT_CALL 60
%define WRITE_CALL 1
%define stdin 0
%define stdout 1
%define stderr 2

global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global parse_uint
global parse_int
global read_word
global string_copy

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, EXIT_CALL         ; exit syscall number
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax                ; zero rax -- return value

    .loop:                      ; start of main loop
        cmp byte [rdi+rax], 0   ; rdi+rax -- pointer to current symbol,
                                ; comparing current symbol with null-terminator
        je  .end                ; if equals -- jump to end
        inc rax                 ; otherwise rax = rax + 1
        jmp .loop               ; and go to next iteration

    .end:
        ret                     ; now rax should contain length of the string


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    mov rsi, stdout

; rdi -- string
; rsi -- descriptor
general_print:
    push rsi
    push rdi
    call string_length          ; pointer to string is already in rdi
    pop rdi
    pop rsi

    mov rdx, rax                ; move length to rdx
    mov rax, rsi                ; temp store rsi
    mov rsi, rdi                ; move pointer to rsi
    mov rdi, rax                ; descriptor
    mov rax, WRITE_CALL         ; write syscall

    syscall                     ; write(rdi, rsi, rdx) -> write(1, ptr, length)
    ret

print_err_string:
    mov rsi, stderr
    jmp general_print

; Принимает код символа и выводит его в stdout
print_char:
    push rdi                    ; locate symbol in stack
    mov rdi, stdout
    mov rsi, rsp                ; pointer to symbol
    mov rdx, 1
    mov rax, WRITE_CALL

    syscall                     ; write (rdi, rsi, rdx) -> write (1, symbol, 1)

    pop rdi                     ; restore rsp and stack
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor r8, r8                  ; clear important registers
    xor rdx, rdx

    push rdx                    ; locate 24-byte buffer in stack
    push rdx                    ; <... 24 zeros ...>
    push rdx

    mov r8, rsp                 ; save current rsp

    mov rax, rdi                ; prepare number
    mov r10, 10                 ; divider

    .loop:
        xor rdx, rdx            ; zero remainder register
        div r10                 ; divide rdx:rax / r10
        add rdx, 0x30           ; convert to ASCII code
        dec rsp                 ; proceed to next symbol
        mov byte [rsp], dl      ; we want to copy only one byte

        cmp rax, 0              ; check if there something more
        jnz .loop               ; iteration

    mov rdi, rsp                ; pass pointer to string to rdi
    push r8
    call print_string           ; print our zero terminated string
    pop r8

    mov rsp, r8                 ; restore rsp
    pop rax
    pop rax
    pop rax                     ; cleanup stack
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    cmp rdi, 0
    jge print_uint

    xor rdx, rdx

    push rdx                    ; locate 24-byte buffer in stack
    push rdx                    ; <... 24 zeros ...>
    push rdx

    mov r8, rsp                 ; save current rsp

    neg rdi
    mov rax, rdi                ; prepare number
    mov r10, 10                 ; divider

    .loop:
        xor rdx, rdx            ; zero remainder register
        div r10                 ; divide rdx:rax / r10
        add rdx, 0x30           ; convert to ASCII code
        dec rsp                 ; proceed to next symbol
        mov byte [rsp], dl      ; we want to copy only one byte

        cmp rax, 0              ; check if there something more
        jnz .loop               ; iteration

    dec rsp
    mov byte [rsp], '-'

    mov rdi, rsp                ; pass pointer to string to rdi
    push r8
    call print_string           ; print our zero terminated string
    pop r8

    mov rsp, r8                 ; restore rsp
    pop rax
    pop rax
    pop rax                     ; cleanup stack

    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax                ; rdi - first, rsi - second

    push rdi
    push rsi

    call string_length

    pop rdi
    pop rsi

    push rdi
    push rsi
    push rax

    call string_length

    pop r8
    pop rdi
    pop rsi

    cmp rax, r8
    jnz .not_eq

    xor rcx, rcx
    xor r8, r8
    .loop:
        cmp byte [rdi+rcx], 0
        je  .eq
        mov r8b, byte [rdi+rcx]
        cmp r8b, byte [rsi+rcx]
        jnz .not_eq
        inc rcx
        jmp .loop

    .not_eq:
        xor rax, rax
        ret
    .eq:
        mov rax, 1
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax                ; zero return value

    push rax                    ; allocate buffer in stack

    mov rdi, stdin              ; stdin desc
    mov rsi, rsp                ; pointer to buf
    mov rdx, 1                  ; only one symbol
    syscall                     ; read(0, rsp, 1) (zero code of read is already in rax)

    pop rax                     ; buffer to return register
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:                      ; rdi -- buffer start, rsi -- buffer length
    ;mov r12, rdi                ; store it somewhere
    xor rcx, rcx                ; zero counter

    cmp rsi, rcx
    je  .overflow               ; test zero buffer
    dec rsi                     ; space for zero terminator

    .loop:
        push rdi                ; try to read char
        push rsi
        push rcx

        call read_char

        pop rcx
        pop rsi
        pop rdi

        cmp rax, 0              ; End of input
        je .end

        cmp rax, 0x20           ; Handle whitespace char
        je  .white
        cmp rax, 0x9
        je  .white
        cmp rax, 0xA
        je  .white

        cmp rcx, rsi            ; Is it possible to locate another char
        je .overflow

        mov byte [rdi+rcx], al  ; locate char into memory
        inc rcx                 ; update counter
        jmp .loop

        .white:
            cmp rcx, 0          ; Is it word start?
            je  .loop
            jmp .end

    .overflow:
        xor rax, rax
        ret

    .end:
        xor rax, rax
        mov byte [rdi+rcx], al
        mov rax, rdi
        mov rdx, rcx
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax                ; zero ret
    xor rdx, rdx

    xor r8, r8                  ; zero register

    .loop:
        cmp byte [rdi+rdx], '0' ; Is it digit?
        jb  .end
        cmp byte [rdi+rdx], '9'
        ja  .end

        mov r8b, byte [rdi+rdx]
        inc rdx

        sub r8, 0x30           ; convert to number
        imul rax, 10            ; process new digit
        add rax, r8

        jmp .loop

    .end:
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx

    cmp byte [rdi], '-'
    je  .negative

    jmp parse_uint

    .negative:
        inc rdi
        call parse_uint
        neg rax
        inc rdx

    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax            ; just in case
                            ; args: rdi -- pointer to string
                            ;       rsi -- pointer to buffer
                            ;       rdx -- buffer length
    push rdi                ; saving params
    push rsi
    push rdx

    call string_length      ; rax -- string length without 0-term

    pop rdx                 ; restoring params
    pop rsi
    pop rdi

    inc rax                 ; rax -- string length with 0-term
    cmp rax, rdx
    ja  .buffer_overflow    ; jump if above

    xor rcx, rcx                    ; zero counter register
    .loop:                          ; main loop
        mov r8b, byte [rdi + rcx]    ; copying through r8, using only 8 bits
        mov byte [rsi + rcx], r8b
        inc rcx
        cmp rcx, rax
        jb  .loop           ; jump if below

    .buffer_overflow:
        xor rax, rax        ; zero rax -- returning value
        ret
