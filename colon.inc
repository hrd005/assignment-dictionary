%define lptr 0
%macro colon 2

    %2:
        dq lptr
        db %1, 0

    %define lptr %2
%endmacro