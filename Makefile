.PHONY: all clean

all: main.o dict.o lib.o
	ld -o main $^

main.o: main.asm colon.inc words.inc
	nasm -felf64 $<

%.o: %.asm
	nasm -felf64 $@ $<

clean:
	rm -f main.o lib.o dict.o main
